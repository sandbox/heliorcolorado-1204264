<?php

/**
 * Implements hook_field_fairy_info().
 */
function hook_field_fairy_info() {
  $path = drupal_get_path('module', 'mymodule');
  
  $plugins['mymodule_image'] = array(
    'handler' => 'field_fairy',
    'path' => $path . '/fields',
    'file' => 'image.inc',
    
    // abstract will prevent this plugin from being directly loaded, but can
    // still participate in the child-parent chaining. This can be helpful when
    // your defining several fields that share many components.
    'abstract' => TRUE,
    
    // basically turn off the plugin
    'disabled' => TRUE,
    
    
    // This basically tells the Field Fairy that this plugin implements
    // hook_field_widget_info(). MOST typical hooks are supported and declaring
    // them when your plugin implements them is crucial to having them execute.
    'field_widget_info' => TRUE,
    
    // .... etc.
  );
    
  return $plugins;
}
