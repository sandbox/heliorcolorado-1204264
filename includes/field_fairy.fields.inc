<?php

/**
 * Implements hook_field_fairy_info().
 */
function field_fairy_field_fairy_info() {
  $path = drupal_get_path('module', 'field_fairy');
  
  $fields['FieldFairy'] = array(
    'handler' => 'FieldFairy',
    'path' => $path . '/includes',
    'file' => 'FieldFairy.inc',
    'abstract' => TRUE,
  );
  
  $fields['FieldFairyText'] = array(
    'handler' => 'FieldFairyText',
    'path' => $path . '/includes',
    'file' => 'FieldFairyText.inc',
    'field_info' => TRUE,
    'field_schema' => TRUE,
    'field_settings_form' => TRUE,
    'field_validate' => TRUE,
    'field_is_empty' => TRUE,
    'field_formatter_info' => TRUE,
    'field_formatter_view' => TRUE,
    'field_widget_info' => TRUE,
    'field_widget_settings_form' => TRUE,
    'field_widget_form' => TRUE,
    'field_widget_error' => TRUE,
  );
    
  return $fields;
}
