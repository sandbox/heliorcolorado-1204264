<?php

class FieldFairyText extends FieldFairy {
  
  /**
   * Implements hook_field_info().
   */
  function field_info() {
    return array(
      'label' => t('Field Fairy Text'),
      'description' => t('This field stores varchar text in the database.'),
      'settings' => array('max_length' => 255),
      'default_widget' => 'FieldFairyText',
      'default_formatter' => 'FieldFairyText',
    );
  }
  
  /**
   * Implements hook_field_schema().
   */
   function field_schema($field) {
     return array(
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => $field['settings']['max_length'],
          'not null' => FALSE,
        ),
      ),
     );
   }
   
  /**
   * Implements hook_field_settings_form().
   */
  function field_settings_form($field, $instance, $has_data) {
    $settings = $field['settings'];
    $form = array();

    $form['max_length'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum length'),
      '#default_value' => $settings['max_length'],
      '#required' => TRUE,
      '#description' => t('The maximum length of the field in characters.'),
      '#element_validate' => array('_element_validate_integer_positive'),
      // @todo: If $has_data, add a validate handler that only allows
      // max_length to increase.
      '#disabled' => $has_data,
    );

    return $form;
  }
  
  /**
   * Implements hook_field_validate().
   *
   * Possible error codes:
   * - 'text_value_max_length': The value exceeds the maximum length.
   */
  function field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
    foreach ($items as $delta => $item) {
      if (!empty($item['value'])) {
        if (!empty($field['settings']['max_length']) && drupal_strlen($item['value']) > $field['settings']['max_length']) {
          $errors[$field['field_name']][$langcode][$delta][] = array(
            'error' => "text_{$column}_length",
            'message' => t('%name: the text may not be longer than %max characters.', array('%name' => $instance['label'], '%max' => $field['settings']['max_length'])),
          );
        }
      }
    }
  }
  
  /**
   * Implements hook_field_is_empty().
   */
  function field_is_empty($item, $field) {
    return !isset($item['value']) || $item['value'] === '';
  }
  
  /**
   * Implements hook_field_formatter_info().
   */
  function field_formatter_info() {
    return array(
      'label' => t('Field Fairy Text'),
      'field types' => array('FieldFairyText'),
      'settings' => array('enforce_plain_text' => 0),
    );
  }
  
  /**
   * Implements hook_field_formatter_view().
   */
  function field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
    $element = array();

    foreach ($items as $delta => $item) {
      $element[$delta] = array('#markup' => strip_tags($item['value']));
    }

    return $element;
  }
  
  /**
   * Implements hook_field_widget_info().
   */
  function field_widget_info() {
    return array(
      'label' => t('Field Fairy Text'),
      'field types' => array('FieldFairyText'),
      'settings' => array('size' => 60),
    );
  }
  
  /**
   * Implements hook_field_widget_settings_form().
   */
  function field_widget_settings_form($field, $instance) {
    $settings = $widget['settings'];

    $form['size'] = array(
      '#type' => 'textfield',
      '#title' => t('Size of textfield'),
      '#default_value' => $settings['size'],
      '#required' => TRUE,
      '#element_validate' => array('_element_validate_integer_positive'),
    );

    return $form;
  }
   
  /**
   * Implements hook_field_widget_form().
   */
  function field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
    $element += array(
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : '',
      '#size' => $instance['widget']['settings']['size'],
      '#attributes' => array('class' => array('text-full')),
    );
    return $element;
  }

  /**
   * Implements hook_field_widget_error().
   */
  function field_widget_error($element, $error, $form, &$form_state) {
    form_error($element[$element['#columns'][0]], $error['message']);
  }
}
